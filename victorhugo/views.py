from django.shortcuts import render, render_to_response
#from django.core.mail import send_mail
#from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
#from django.core.urlresolvers import reverse_lazy
from django.template import RequestContext
from django.conf import settings


# Create your views here.
def home(request):
	return render(request, "home.html")

def about(request):
        return render(request, "about.html", {})