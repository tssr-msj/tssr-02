# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import sexe, matiere, classe, salle, type_evaluation, eleve, evaluation, enseignant, cours, passer_evaluation, enseigner, presence

class AdminEnseignement(admin.ModelAdmin):
	list_display=('enseignant', 'matiere')

class AdminPersonne(admin.ModelAdmin):
	list_display=('nom', 'prenom', 'sexe')

class AdminEleve(admin.ModelAdmin):
	list_display=('nom', 'prenom', 'sexe', 'date_naissance', 'classe')

class AdminEnseignant(admin.ModelAdmin):
	list_display=('nom', 'prenom', 'sexe','telephone')

class AdminCours(admin.ModelAdmin):
	list_display=('code_court','description','date','heure_debut','heure_fin','salle','classe','enseignant')

class AdminPresence(admin.ModelAdmin):
	def date_releve(self, obj):
		return obj.date.strftime("%d %m %Y")
	list_display=('date_releve', 'eleve', 'present')

admin.site.register(sexe)
admin.site.register(classe)
admin.site.register(salle)
admin.site.register(type_evaluation)
admin.site.register(cours,AdminCours)
admin.site.register(enseigner, AdminEnseignement)
admin.site.register(matiere)
admin.site.register(enseignant,AdminEnseignant)
admin.site.register(presence,AdminPresence)
admin.site.register(eleve,AdminEleve)
admin.site.register(passer_evaluation)
admin.site.register(evaluation)


