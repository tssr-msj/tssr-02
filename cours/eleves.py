# -*- coding: utf-8 -*-
from django import forms
from django.forms import Form, ModelForm
from .models import eleve, sexe
from django.conf import settings


class EleveForm(forms.ModelForm):
    class Meta:
        model = eleve,sexe
        fields = ('prenom', 'nom', 'date_naissance','sexe')

    def __init__(self, *args, **kwargs):
        super(EleveForm, self).__init__(*args, **kwargs)    
        self.fields['prenom'].required=False
        self.fields['prenom'].label = "Prénom"
        self.fields['nom'].required=False
        self.fields['nom'].label = "Nom"
        self.fields['date_naissance'].required=False
        self.fields['date_naissance'].label = "Date de naissance"
        self.fields['sexe'].required=False
        self.fields['sexe'].label = "Sexe"
      
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.form_class = 'form-horizontal'
    helper.form_id = 'user_form'
    helper.label_class = 'col-md-6'
    helper.field_class = 'col-md-6' 
    helper.form_tag = False
    helper.add_input(Submit('submit', 'Submit'))
    helper.layout = Layout(
        TabHolder(
            Tab(
                'Contact',
                'prenom',
                'nom',
                'date_naissance',
                'sexe',
            ),
        ),  
       )